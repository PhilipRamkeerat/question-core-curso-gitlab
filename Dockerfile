FROM registry.gitlab.com/philipramkeerat/question-core-curso-gitlab:dependencies

copy . .

run mkdir -p assets/static \
    && python manage.py collectstatic --noinput